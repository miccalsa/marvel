import React from 'react';
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import logo from 'resources/images/marvel_logo.svg';

import './styles.css'

function Header() {
  return (
    <header>
      <img src={logo} alt="marvelpedia" />
      <div className="header-search">
        <h1>Marvelpedia</h1>
        <form>
          <div className="search-form">
            <label htmlFor="marvelSearch">
              <FontAwesomeIcon icon={faSearch} />
            </label>
            <input type="search" name="marvelSearch" id="marvelSearch" />
          </div>
        </form>
      </div>
    </header>
  );
}

export default Header;
