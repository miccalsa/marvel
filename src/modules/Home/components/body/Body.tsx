import React from 'react';

import Comic from 'modules/Home/components/comic/Comic';

import './styles.css';

function Body(props: Results) {
  const comicList = props.results.map((comic, index) => 
    <Comic 
      key={index} 
      title={comic.title} 
      picture={`${comic.thumbnail.path}/portrait_fantastic.${comic.thumbnail.extension}`} 
      description={`${comic.description && comic.description.length ? comic.description : 'No description available'}`} 
    />
  );

  return (
  <div className="main-section">
    <div className="results-section">
      <div className="grid">
        { comicList }
      </div>
    </div>
  </div>
  );
}

interface Results {
  results: Array<ComicItem>
}

interface ComicItem {
  description: string;
  title: string;
  thumbnail: Image;
}

interface Image {
  path: string;
  extension: string;
}

export default Body;
