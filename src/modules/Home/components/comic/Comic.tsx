import React from 'react';

import './styles.css';

function Comic(props: Comic) {
  return (
    <div className="item">
        <div className="content">
          <div className="title">
            <h3>{ props.title }</h3>
          </div>
          <img className="photothumb" src={ props.picture } alt={ props.title } />
          <div className="desc">
            <p>{ props.description }</p>
          </div>
        </div>
      </div>
  );
}

interface Comic {
  title: string;
  description: string;
  picture: string;
}

export default Comic;
