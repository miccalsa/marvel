import React, { useState, useEffect } from 'react';
import axios from 'axios';
import md5 from 'crypto-js/md5';

import Header from 'modules/Home/components/header/Header';
import Body from 'modules/Home/components/body/Body';

function Layout() {
  const [comicList, setComicList] = useState([]);

  function buildUrl() {
    const timestamp = new Date().getTime();
    const privateKey = process.env.REACT_APP_PRIV_KEY || '';
    const publicKey = process.env.REACT_APP_PUBLIC_KEY || '';
    const hash = md5(timestamp + privateKey + publicKey).toString();
    const url = `http://gateway.marvel.com:80/v1/public/comics?ts=${timestamp}&apikey=${publicKey}&hash=${hash}&limit=20&offset=${Math.floor(Math.random() * 100)}`;
    return axios.get(url);
  }

  useEffect(() => {
    buildUrl()
    .then(response => {
      console.log(response.data);
      console.log(response.data.data.results);
      setComicList(response.data.data.results);
    }).catch((error) => console.log(error));
  }, []);

  return (
    <>
      <Header />
      <Body results={comicList} />
    </>
  );
}

export default Layout;