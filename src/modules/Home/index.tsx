import React from 'react';

import Layout from 'modules/Home/components/Layout';

function Home() {
  return (
    <Layout />
  );
}

export default Home;
